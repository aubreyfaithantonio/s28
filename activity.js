// CRUD OPERATION

// 1. CREATE/INSERT DOCUMENT
// [insertOne]
db.rooms.insert({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})
// [insertOne END]

// [insertMany]
db.rooms.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
])
// [insertMany END]

// 2. READ/FIND DOCUMENT
// [findOne]
db.rooms.find({name: "double"})
// [findOne END]

// 3. UPDATE DOCUMENT
// [UpdateOne]
db.rooms.updateOne(
	{
		name: "queen"
	},
	{
		$set: {
			rooms_available: 0
		}
	}
)
// [UpdateOne END]

// 4. DELETE DOCUMENT
// [deleteMany]
db.rooms.deleteMany({
	rooms_available: 0
})
// [deleteMany END]